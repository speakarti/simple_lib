from ..simple_lib import sub


def test_simple_sub():
    assert sub(5, 7) == -2


def test_sub_with_zero():
    assert sub(4, 0) == 4


def test_sub_with_two_digits():
    assert sub(12, 45) == -33
